# Taints - Node
* kubectl taint nodes node-name key=value:taint-effect
* kubectl taint nodes node-name key=value:NoSchedule
* kubectl taint nodes node-name key=value:PreferNoSchedule
* kubectl taint nodes node-name key=value:NoExecute

# Tolerations - PODS
* kubectl taint node node1 app=blue:NoSchedule
* `pod-definition.yml`
```yaml
apiVersion:
kind: Pod
metadata:
  name: myapp-pod
spec:
  containers:
    - name: nginx-container
      image: nginx

  tolerations:
  - key: "app"
    operator: "Equal"
    value: "blue"
    effect: "NoSchedule"
```

