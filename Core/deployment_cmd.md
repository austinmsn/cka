# commands

> kubectl get all

* Create an NGINX Pod
`kubectl run ngnix --image=nginx`

* Generate POD Manifiest YAML file (-o yaml). Don't create it (--dry-run)
`kubectl run nginx --image-nginx --dry-run=client -o yaml`

* Create a deployment
`kubectl create deployment --image=nginx nginx`

* Generate Deployment YAML file (-o yaml). Don't create it (--dry-run)
`kubectl create deployment --image=nginx nginx --dry-run=client -o yaml`

* Save it to a file, make necessary changes to the file (for example, adding more replicas) and then create the deployment.
`kubectl create -f nginx-deployment.yaml`

* In k8s version 1.19+, we can specify the --replicas option to create a deployment with 4replicas.
`kubectl create deployment --image=nginx nginx --replicas=4 --dry-run=client -o yaml > nginx-deployment.yaml`

* `deployment.yaml` sample
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: httpd-frontend
spec:
  replicas: 3
  selector:
    matchLabels:
      name: httpd-container
  template:
    metadata:
      labels:
       name: httpd-container
    spec:
      containers:
        - name: httpd:2.4-alpine
          image: busybox888
          command: 
          - sh
          - "-c" 
          - echo Hello Kubernetes! && sleep 3600
```