# definition file (yaml)
```yaml
apiVersion: v1
kind: Service
metadata:
  name: myapp-service

spec:
  type: LoadBalancer
  ports:
   - targetPort: 80
     port: 80
     nodePort: 30008
```

# sample from kubernetes
```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector:
    app.kubernetes.io/name: MyApp
  ports:
    - protocol: TCP
      port: 80
      targetPort: 9376
```

# solution

> kubectl get service
> kubectl get svc
> kubectl get deploy