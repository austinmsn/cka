# command

> kubectl get pods -n kube-system
> kubectl create -f pod-definition.yml

> kubectl create -f pod-definition.yml
> kubectl create -f pod-definition.yml --namespace=dev

* sample yaml
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  namespace: dev
  labels:
    app: myapp
    type: front-end
spec:
  containers:
  - name: nginx-container
    image: nginx
```

* example yaml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: dev
```

> kubectl create -f namespace-dev.yml
> kubectl create namespace dev

> kubectl get pods --namespace=dev
> kubectl get pods
> kubectl get pods --namespace=prod

> kubectl config set-context $(kubectl config current-context) --namespace=dev
> kubectl get pods
> kubectl get pods --namespace=default
> kubectl get pods --namespace=prod

> kubectl config set-context $(kubectl config current-context) --namespace=prod
> kubectl get pods --namespace=dev
> kubectl get pods --namespace=default
> kubectl get pods

> kubectl get pods --all-namespaces

# sample Compute-quota.yaml
```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: compute-quota
  namespace: dev
spec:
  hard:
    pods: "10"
    requests.cpu: "4"
    requests.memory: 5Gi
    limits.cpu: "10"
    limits.memory: 10Gi
```

# solution

> kubectl get namespaces
> kubectl get ns
> kubectl get pods --namespace=research
> kubectl get pods -n=research

> kubectl run redis --image=redis -n=finance
> kubectl get pods -n=finance

> kubectl get pods --all-namespaces
> kubectl get pods -A

> kubectl get pods -n=marketing
> kubectl get svc -n=marketing

> kubectl get svc -n=dev