# Imperative

* Create Objects
> kubectl run --image=nginx nginx
> kubectl create deployment --image=nginx nginx
> kubectl expose deployment nginx --port 80

* Update Objects
> kubectl edit deployment nginx
> kubectl scale deployment nginx --replicas=5
> kubectl set image deployment nginx nginx=nginx:1.18

* Create Object
> kubectl create -f nginx.yaml
> kubectl replace -f nginx.yaml
> kubectl delete -f nginx.yaml

* Update Object
> kubectl edit deployment nginx
> kubectl replace -f nginx.yaml
> kubectl replace --force -f nginx.yaml
> kubectl create -f nginx.yaml
`Error from server (AlreadyExists) ...`
> kubectl replace -f nginx.yaml
`Error from server (Conflict) ...`

## ex file

* nginx.yaml
```yaml
apiVersion: v1
kind: Pod

metadata:
  name: myapp-pod
  labels:
    app: myapp
    type: front-end
spec:
  containers:
  - name: nginx-container
    image: nginx
```

*  pod-definition
```yaml
apiVersion: v1
kind: Pod

metadata:
  name: myapp-pod
  labels:
    app: myapp
    type: front-end
spec:
  containers:
  - name: nginx-container
    image: nginx
status:
  conditions:
  - lastProbeTime: null
    status: "True"
    type: Initialized
```

# Delcarative

* Create Object
> kubectl apply -f nginx.yaml
> kubectl apply -f /path/to/config-files

* Update Object
> kubectl apply -f nginx.yaml