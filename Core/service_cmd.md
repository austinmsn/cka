# definition file (yaml)

```yaml
apiVersion: v1
kind: Service
metadata:
  name: myapp-service

spec:
  type: NodePort
  ports:
    - targetPort: 80
      port: 80
      nodePort: 30008
  selector:
    app: myapp
    type: front-end
```

# example file (yaml)
```yaml
apiVersion: v1
kind: Service
metadata:
  name: back-end

spec:
  type: ClusterIP
  ports:
   - targetPort: 80
     port: 80
  selector:
    app: myapp
    type: back-end
```

> kubectl create -f service-definition.yml
> kubectl get services